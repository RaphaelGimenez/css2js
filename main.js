(function () {
  const rawCssElement = document.getElementById('rawCss');
  const jsOutputElement = document.getElementById('jsOutput');
  let wasDash = false;
  rawCssElement.addEventListener('input', (e) => {
    const { value } = e.target;
    const sanatizedValue = value.trim().replace('/\n|\r/g', '').split('');
    let jsOutput = sanatizedValue.reduce((reducer, currValue, index) => {
      console.log(currValue.trim());
      switch (currValue.trim()) {
        case '':
          console.log('nothing');
          return reducer;
        case ':':
          return `${reducer}${currValue} "`;
        case '-':
          console.log('-');
          wasDash = true;
          return reducer;
        case ';':
          if (index !== sanatizedValue.length - 1) {
            return `${reducer}",\n  `;
          } else {
            return `${reducer}",`;
          }
        default:
          console.log(currValue);
          return `${reducer}${isCapitalLetter(wasDash, currValue)}`;
      }
    }, '');

    console.log(jsOutput);
    jsOutputElement.value = `{\n  ${jsOutput}\n}`;
  });

  const isCapitalLetter = (prevValue, currValue) => {
    const value = prevValue ? currValue.toUpperCase() : currValue;
    wasDash = false;
    return value;
  };
})();
